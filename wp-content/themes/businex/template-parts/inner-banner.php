<?php
/**
* Template for Inner Banner Section for all the inner pages
*
* @since Businex 1.0.0
*/
?>

<section class="wrapper wrap-inner-banner" >
	<div >
<?php 
echo do_shortcode('[smartslider3 slider=1]');
?>
		<?php 
		if( !is_front_page()){
			businex_breadcrumb();
		}
	?>
</section>