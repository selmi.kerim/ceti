<?php
/**
 * Theme functions and definitions
 *
 * @package business_aid
 */

if ( ! function_exists( 'business_aid_enqueue_styles' ) ) :
	/**
	 * @since Business Aid 1.0.0
	 */
	function business_aid_enqueue_styles() {
		wp_enqueue_style( 'business-aid-style-parent', get_template_directory_uri() . '/style.css' );
		wp_enqueue_style( 'business-aid-style', get_stylesheet_directory_uri() . '/style.css', array( 'business-aid-style-parent' ), '1.0.0' );
		wp_enqueue_style( 'business-aid-google-fonts', '//fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700,700i,800,900', false );
	}

endif;
add_action( 'wp_enqueue_scripts', 'business_aid_enqueue_styles', 99 );

