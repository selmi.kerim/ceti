=== Business Aid ===

Contributors: keonthemes
Tags: blog, portfolio, education, grid-Layout, two-columns, flexible-header, left-sidebar, right-sidebar, custom-background, custom-colors, custom-header, custom-logo, custom-menu, featured-images, full-width-template, post-formats, rtl-language-support, theme-options, sticky-post, threaded-comments, translation-ready

Requires at least: 4.7
Tested up to: 4.9.8
Stable tag: 1.0.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== Description ==

Business Aid is a beautiful and elegant child theme of Businex. It adds support for Montserrat font and Red color style. Businex is a clean, responsive theme that&apos;s versatile and easy to use. Suitable for both corporate and creative businesses, this elegant Theme design gives you maximum flexibility. The clean and lean code means that it loads quickly too, so you won&apos;t keep your clients waiting. Businex is Ultimate WordPress Theme with stunning features like, WooCommerce compatibility, Translation Ready, Child Theme ready, Cross-Browser compatibility, Right-to-Left Language Support, 3 different Header Layouts, 2 Footer Layouts, 2 Slider options (Page slider and Post slider), Sidebar layout options, Post Layout options, Single Page Layout options, Custom page templates, thin Font Icons, Google Fonts, Unlimited Color Options and many more. This versatile theme is managed from Live Customizer, that lets you edit your content in real-time. One-click demo import, Regular updates, Well documented and Support through Email, Forum and Chat are the key factors. Business Aid Theme Demo: https://keonthemes.com/theme-demo/?id=MjIzN3xidXNpbmVzcy1haWR8QnVzaW5lc3MgQWlk

== License ==

Business Aid WordPress Theme, Copyright (C) 2018, Keon Themes
Business Aid is distributed under the terms of the GNU GPL

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Credits ==

== Images ==
 
* Screenshot Image:
License: CC0 Public Domain (CC0 1.0)
Source: https://pxhere.com/en/photo/115778

== Changelog ==

= 1.0.0 =
* Initial release.