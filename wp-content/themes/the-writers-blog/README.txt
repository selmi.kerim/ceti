=== The Writers Blog ===
Contributors: ThemesEye
Tags: left-sidebar, right-sidebar, one-column, two-columns, grid-layout, custom-colors, custom-background, custom-logo, custom-menu, custom-header, editor-style, featured-images, footer-widgets, sticky-post, full-width-template, rtl-language-support, theme-options, threaded-comments, translation-ready, blog, portfolio, e-commerce
Requires at least: 4.7
Tested up to: 5.0.2
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Writers Blog is an elegant, feature-rich, sleek and versatile blog WordPress theme that makes the best use of the online space to enlighten visitors on various interesting topics. This multipurpose theme can be used as a blog, informative website, portfolio, landing page, resume, online journal, non-profits, consultants and for diverse niches.

== Description ==

The Writers Blog is an elegant, feature-rich, sleek and versatile blog WordPress theme that makes the best use of the online space to enlighten visitors on various interesting topics. This multipurpose theme can be used as a blog, informative website, portfolio, landing page, resume, online journal, non-profits, consultants and for diverse niches. The Writers Blog is essentially for writers, journalists, authors, poets and similar people belonging to literature field to beautifully present their thoughts to make them most impactful. It is responsive, cross-browser compatible, translation ready and retina ready. It has a welcoming slider on the homepage to impress visitors at the very first sight. Its design is well thought to keep readers focus on content. This blog WordPress theme is developed on the latest WordPress version to make your website modern and advanced. It supports various post formats and offers various design options for different sections. The theme is highly customizable without requiring any previous coding knowledge; this makes you immensely powerful as a website owner. Its colour scheme can be chosen from a wide spectrum of colours; social media icons are there to give you fame. Its SEO is well working to get good traffic.   

== Changelog ==

= 0.1 =
	Initial Version Released.

== Resources ==

The Writers Blog WordPress Theme, Copyright 2019 ThemesEye
The Writers Blog is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see . http://www.gnu.org/licenses/

The Writers Blog WordPress Theme is derived from Twenty Seventeen WordPress Theme, Copyright 2016 WordPress.org
Twenty Seventeen WordPress Theme is distributed under the terms of the GNU GPL

The Writers Blog bundles the following third-party resources:

* CSS bootstrap.css
  Copyright 2011-2018 The Bootstrap Authors
  https://github.com/twbs/bootstrap/blob/master/LICENSE

* JS bootstrap.js
  Copyright 2011-2018 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  https://github.com/twbs/bootstrap/blob/master/LICENSE

* The Font Awesome font is licensed under the SIL OFL 1.1:
  http://scripts.sil.org/OFL
  Font Awesome CSS, LESS, and Sass files are licensed under the MIT License:
  https://opensource.org/licenses/mit-license.html

* Customizer Pro
  Source: https://github.com/justintadlock/trt-customizer-pro

* HTML5 Shiv, Copyright 2014 Alexander Farkas
  Licenses: MIT/GPL2
  Source: https://github.com/aFarkas/html5shiv

* Pixabay Images
  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pixabay.com/en/service/terms/

  Slider, Copyright MabelAmber
  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pixabay.com/en/feet-shoes-sneakers-legs-1567104/
 
  Post, Copyright StockSnap
  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pixabay.com/en/girl-woman-people-lady-alone-2589554/

  Gallery, Copyright Free-Photos
  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pixabay.com/en/beauty-girl-fashion-hat-sexy-863439/

  Gallery, Copyright Pezibear
  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pixabay.com/en/girl-balloons-child-happy-out-1357485/

  Gallery, Copyright Myriams-Fotos
  License: CC0 1.0 Universal (CC0 1.0)
  Source: https://pixabay.com/en/girl-woman-joy-of-life-dance-2940655/