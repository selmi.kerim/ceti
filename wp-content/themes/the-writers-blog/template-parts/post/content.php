<?php
/**
 * Template part for displaying posts
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="blogger">
		<div class="row m-0">
	        <div class="col-lg-5 col-md-5 pr-0">
	          <?php if(has_post_thumbnail()) { ?>
	          	<div class="post-link">
			      <div class="dateday"><?php echo esc_html( get_the_date( 'd') ); ?></div>
			      <div class="month"><?php echo esc_html( get_the_date( 'M' ) ); ?></div>
			      <div class="year"><?php echo esc_html( get_the_date( 'Y' ) ); ?></div>
			    </div>
	          	<?php the_post_thumbnail(); ?>
	          <?php } ?>
	        </div>
	        <div class="<?php if(has_post_thumbnail()) { ?>col-lg-7 col-md-7 pl-0"<?php } else { ?>col-lg-12 col-md-12"<?php } ?>">
				<div class="box-content">
					<p class="cat-pst"><?php foreach((get_the_category()) as $category) { echo esc_html($category->cat_name) . ' '; } ?></p>
					<h4><?php the_title(); ?></h4>
					<div class="post-info">
					  <i class="fa fa-calendar" aria-hidden="true"></i><span class="entry-date"> <?php echo esc_html(get_the_date()); ?></span>
					  <i class="fa fa-user" aria-hidden="true"></i><span class="entry-author"> <?php the_author(); ?></span>
					  <i class="fas fa-comments"></i><span class="entry-comments"><?php comments_number( __('0 Comments','the-writers-blog'), __('0 Comments','the-writers-blog'), __('% Comments','the-writers-blog') ); ?></span>
					</div>
					<p><?php $excerpt = get_the_excerpt(); echo esc_html( the_writers_blog_string_limit_words( $excerpt,25 ) ); ?></p>
					<div class="row">
						<div class="col-lg-7 col-md-12">
							<div class="icons">
							    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php  the_permalink(); ?>" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
							    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php  the_permalink(); ?>" target="_blank"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
							    <a href="https://plus.google.com/share?url=<?php  the_permalink(); ?>" target="_blank"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a>
							    <a href="https://twitter.com/share?url=<?php  the_permalink(); ?>" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a>
							    <a href="http://www.instagram.com/submit?url=<?php  the_permalink(); ?>" target="_blank"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="col-lg-5 col-md-12">
							<div class ="aboutbtn">
					          <a href="<?php the_permalink(); ?>"><?php esc_html_e('VIEW POST','the-writers-blog'); ?><i class="fas fa-angle-double-right"></i></a>
					        </div>
						</div>
					</div>
				</div>
	        </div>
	    </div>
	</div>
</div>
