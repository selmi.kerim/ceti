<?php
/**
 * Displays footer site info
 */

?>
<div class="site-info">
	<p><?php echo esc_html(get_theme_mod('the_writers_blog_footer_text',__('Design & Developed By','the-writers-blog'))); ?> <?php the_writers_blog_credit(); ?></p>
</div>