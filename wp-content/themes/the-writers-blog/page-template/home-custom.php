<?php
/**
 * Template Name: Home Custom Page
 */

get_header(); ?>

<?php do_action( 'the_writers_blog_before_slider' ); ?>

<?php if( get_theme_mod('the_writers_blog_slider_arrows') != ''){ ?>

  <section id="slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel"> 
      <?php $pages = array();
        for ( $count = 1; $count <= 3; $count++ ) {
          $mod = intval( get_theme_mod( 'the_writers_blog_slide_page' . $count ));
          if ( 'page-none-selected' != $mod ) {
            $pages[] = $mod;
          }
        }
        if( !empty($pages) ) :
        $args = array(
          'post_type' => 'post',
          'post__in' => $pages,
          'orderby' => 'post__in'
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) :
          $i = 1;
      ?>
      <div class="carousel-inner" role="listbox">
        <?php  while ( $query->have_posts() ) : $query->the_post(); ?>
        <div <?php if($i == 1){echo 'class="carousel-item active"';} else{ echo 'class="carousel-item"';}?>>
          <img src="<?php the_post_thumbnail_url('full'); ?>"/>
          <div class="carousel-caption">
            <div class="inner_carousel">
              <div  class="cats">
                <p><?php foreach((get_the_category()) as $category) { echo esc_html($category->cat_name) . ' '; } ?></p>
              </div>
              <h3><?php the_title();?></h3>
                <div class="post-info">
                  <i class="fa fa-calendar" aria-hidden="true"></i><span class="entry-date"> <?php echo esc_html(get_the_date()); ?></span>
                  <i class="fa fa-user" aria-hidden="true"></i><span class="entry-author"> <?php the_author(); ?></span>
                  <i class="fas fa-comments"></i><span class="entry-comments"><?php comments_number( __('0 Comments','the-writers-blog'), __('0 Comments','the-writers-blog'), __('% Comments','the-writers-blog') ); ?></span>
                </div>
              <p><?php $excerpt = get_the_excerpt(); echo esc_html( the_writers_blog_string_limit_words( $excerpt,16 ) ); ?></p>
              <div class ="readbutton">
               <a href="<?php the_permalink(); ?>"><?php esc_html_e('VIEW POST','the-writers-blog'); ?><i class="fas fa-angle-double-right"></i></a>
              </div>
            </div>
          </div>
        </div>
        <?php $i++; endwhile; 
        wp_reset_postdata();?>
      </div>
      <?php else : ?>
      <div class="no-postfound"></div>
        <?php endif;
      endif;?>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"><i class="fas fa-angle-double-left"></i></span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"><i class="fas fa-angle-double-right"></i></span>
      </a>
    </div> 
    <div class="clearfix"></div>
  </section> 
<?php }?> 

<?php do_action( 'the_writers_blog_after_slider' ); ?>

<div id="home-content" class="container">
  <div class="row">
    <div class="col-lg-9 col-md-9">
      <?php if(get_theme_mod('the_writers_blog_category_section') != ''){ ?>
        <section id="blog-category">
          <?php 
          $catData = get_theme_mod('the_writers_blog_category_section');
            if($catData){              
              $page_query = new WP_Query(array( 'category_name' => esc_html( $catData ,'the-writers-blog')));?>
              <?php while( $page_query->have_posts() ) : $page_query->the_post(); 
                get_template_part( 'template-parts/post/content' );
              endwhile;
              wp_reset_postdata();
            }
          ?>
          <div class="clearfix"></div>
        </section>
      <?php }?>

      <?php do_action( 'the_writers_blog_after_blog_category' ); ?>

      <?php while ( have_posts() ) : the_post();?>
        <?php the_content(); ?>
      <?php endwhile; // End of the loop.
      wp_reset_postdata(); ?>

    </div>
    <div id="sidebox" class="col-lg-3 col-md-3">
      <?php dynamic_sidebar( 'homepage-sidebar' ); ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>