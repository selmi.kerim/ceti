<?php
/**
 * The Writers Blog: Customizer
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function the_writers_blog_customize_register( $wp_customize ) {

	$wp_customize->add_panel( 'the_writers_blog_panel_id', array(
	    'priority' => 10,
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'Theme Settings', 'the-writers-blog' ),
	    'description' => __( 'Description of what this panel does.', 'the-writers-blog' ),
	) );

	$wp_customize->add_section( 'the_writers_blog_general_option', array(
    	'title'      => __( 'Sidebar Settings', 'the-writers-blog' ),
		'priority'   => 30,
		'panel' => 'the_writers_blog_panel_id'
	) );

	// Add Settings and Controls for Layout
	$wp_customize->add_setting('the_writers_blog_layout_settings',array(
        'default' => __('Right Sidebar','the-writers-blog'),
        'sanitize_callback' => 'the_writers_blog_sanitize_choices'	        
	));

	$wp_customize->add_control('the_writers_blog_layout_settings',array(
        'type' => 'radio',
        'label'     => __('Theme Sidebar Layouts', 'the-writers-blog'),
        'description'   => __('This option work for blog page, blog single page, archive page and search page.', 'the-writers-blog'),
        'section' => 'the_writers_blog_general_option',
        'choices' => array(
            'Left Sidebar' => __('Left Sidebar','the-writers-blog'),
            'Right Sidebar' => __('Right Sidebar','the-writers-blog'),
            'One Column' => __('Full Width','the-writers-blog'),
            'Grid Layout' => __('Grid Layout','the-writers-blog')
        ),
	));

	//Social Icons
	$wp_customize->add_section( 'the_writers_blog_social_icons' , array(
    	'title'      => __( 'Social Icons', 'the-writers-blog' ),
		'priority'   => null,
		'panel' => 'the_writers_blog_panel_id'
	) );

	$wp_customize->add_setting('the_writers_blog_facebook_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));	
	$wp_customize->add_control('the_writers_blog_facebook_url',array(
		'label'	=> __('Add Facebook link','the-writers-blog'),
		'section'	=> 'the_writers_blog_social_icons',
		'setting'	=> 'the_writers_blog_facebook_url',
		'type'	=> 'url'
	));

	$wp_customize->add_setting('the_writers_blog_twitter_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));	
	$wp_customize->add_control('the_writers_blog_twitter_url',array(
		'label'	=> __('Add Twitter link','the-writers-blog'),
		'section'	=> 'the_writers_blog_social_icons',
		'setting'	=> 'the_writers_blog_twitter_url',
		'type'	=> 'url'
	));

	$wp_customize->add_setting('the_writers_blog_youtube_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));	
	$wp_customize->add_control('the_writers_blog_youtube_url',array(
		'label'	=> __('Add Youtube link','the-writers-blog'),
		'section'	=> 'the_writers_blog_social_icons',
		'setting'	=> 'the_writers_blog_youtube_url',
		'type'	=> 'url'
	));

	$wp_customize->add_setting('the_writers_blog_googleplus_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));	
	$wp_customize->add_control('the_writers_blog_googleplus_url',array(
		'label'	=> __('Add Google Plus link','the-writers-blog'),
		'section'	=> 'the_writers_blog_social_icons',
		'setting'	=> 'the_writers_blog_googleplus_url',
		'type'	=> 'url'
	));

	$wp_customize->add_setting('the_writers_blog_linkedin_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));
	$wp_customize->add_control('the_writers_blog_linkedin_url',array(
		'label'	=> __('Add Linkedin link','the-writers-blog'),
		'section'	=> 'the_writers_blog_social_icons',
		'setting'	=> 'the_writers_blog_linkedin_url',
		'type'	=> 'url'
	));

	//home page slider
	$wp_customize->add_section( 'the_writers_blog_slider' , array(
    	'title'      => __( 'Slider Settings', 'the-writers-blog' ),
		'priority'   => null,
		'panel' => 'the_writers_blog_panel_id'
	) );

	$wp_customize->add_setting('the_writers_blog_slider_arrows',array(
        'default' => 'true',
        'sanitize_callback'	=> 'sanitize_text_field'
	));
	$wp_customize->add_control('the_writers_blog_slider_arrows',array(
     	'type' => 'checkbox',
      	'label' => __('Show / Hide slider','the-writers-blog'),
      	'section' => 'the_writers_blog_slider',
	));


	$post_list = get_posts();
	$i = 0;	
	$pst_sls[]='Select';
	foreach ($post_list as $key => $p_post) {
		$pst_sls[$p_post->ID]=$p_post->post_title;
	}
	for ( $count = 1; $count <= 4; $count++ ) {
		$wp_customize->add_setting('the_writers_blog_slide_page'.$count,array(
			'sanitize_callback' => 'the_writers_blog_sanitize_choices',
		));
		$wp_customize->add_control('the_writers_blog_slide_page'.$count,array(
			'type'    => 'select',
			'choices' => $pst_sls,
			'label' => __('Select post','the-writers-blog'),
			'section' => 'the_writers_blog_slider',
		));
	}

	$wp_customize->add_section('the_writers_blog_service',array(
		'title'	=> __('Blog Category','the-writers-blog'),
		'description'	=> __('Add Our Blog Category sections below.','the-writers-blog'),
		'panel' => 'the_writers_blog_panel_id',
	));

	$categories = get_categories();
	$cats = array();
	$i = 0;
	$cat_post[]= 'select';
	foreach($categories as $category){
		if($i==0){
			$default = $category->slug;
			$i++;
		}
		$cat_post[$category->slug] = $category->name;
	}

	$wp_customize->add_setting('the_writers_blog_category_section',array(
		'default'	=> 'select',
		'sanitize_callback' => 'sanitize_text_field',
	));
	$wp_customize->add_control('the_writers_blog_category_section',array(
		'type'    => 'select',
		'choices' => $cat_post,
		'label' => __('Select Category to display Latest Post','the-writers-blog'),
		'section' => 'the_writers_blog_service',
	));

	//Footer
	$wp_customize->add_section( 'the_writers_blog_footer' , array(
    	'title'      => __( 'Footer Text', 'the-writers-blog' ),
		'priority'   => null,
		'panel' => 'the_writers_blog_panel_id'
	) );

	$wp_customize->add_setting('the_writers_blog_footer_text',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));	
	$wp_customize->add_control('the_writers_blog_footer_text',array(
		'label'	=> __('Add Copyright Text','the-writers-blog'),
		'section'	=> 'the_writers_blog_footer',
		'setting'	=> 'the_writers_blog_footer_text',
		'type'		=> 'text'
	));


	$wp_customize->get_setting( 'blogname' )->transport          = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport   = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport  = 'postMessage';

	$wp_customize->selective_refresh->add_partial( 'blogname', array(
		'selector' => '.site-title a',
		'render_callback' => 'the_writers_blog_customize_partial_blogname',
	) );
	$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
		'selector' => '.site-description',
		'render_callback' => 'the_writers_blog_customize_partial_blogdescription',
	) );
	
}
add_action( 'customize_register', 'the_writers_blog_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @since The Writers Blog 1.0
 * @see the-writers-blog_customize_register()
 *
 * @return void
 */
function the_writers_blog_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since The Writers Blog 1.0
 * @see the-writers-blog_customize_register()
 *
 * @return void
 */
function the_writers_blog_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Return whether we're on a view that supports a one or two column layout.
 */
function the_writers_blog_is_view_with_layout_option() {
	// This option is available on all pages. It's also available on archives when there isn't a sidebar.
	return ( is_page() || ( is_archive() && ! is_active_sidebar( 'footer-1' ) ) );
}

/**
 * Singleton class for handling the theme's customizer integration.
 *
 * @since  1.0.0
 * @access public
 */
final class the_writers_blog_Customize {

	/**
	 * Returns the instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return object
	 */
	public static function get_instance() {

		static $instance = null;

		if ( is_null( $instance ) ) {
			$instance = new self;
			$instance->setup_actions();
		}

		return $instance;
	}

	/**
	 * Constructor method.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function __construct() {}

	/**
	 * Sets up initial actions.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function setup_actions() {

		// Register panels, sections, settings, controls, and partials.
		add_action( 'customize_register', array( $this, 'sections' ) );

		// Register scripts and styles for the controls.
		add_action( 'customize_controls_enqueue_scripts', array( $this, 'enqueue_control_scripts' ), 0 );
	}

	/**
	 * Sets up the customizer sections.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  object  $manager
	 * @return void
	 */
	public function sections( $manager ) {

		// Load custom sections.
		load_template( trailingslashit( get_template_directory() ) . '/inc/section-pro.php' );

		// Register custom section types.
		$manager->register_section_type( 'the_writers_blog_Customize_Section_Pro' );

		// Register sections.
		$manager->add_section(
			new the_writers_blog_Customize_Section_Pro(
				$manager,
				'example_1',
				array(
					'priority' => 9,
					'title'    => esc_html__( 'The Writers Blog Pro', 'the-writers-blog' ),
					'pro_text' => esc_html__( 'Go Pro', 'the-writers-blog' ),
					'pro_url'  => esc_url('https://www.themeseye.com/wordpress/wordpress-themes-for-blog/'),
				)
			)
		);
	}

	/**
	 * Loads theme customizer CSS.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function enqueue_control_scripts() {

		wp_enqueue_script( 'the-writers-blog-customize-controls', trailingslashit( get_template_directory_uri() ) . '/assets/js/customize-controls.js', array( 'customize-controls' ) );

		wp_enqueue_style( 'the-writers-blog-customize-controls', trailingslashit( get_template_directory_uri() ) . '/assets/css/customize-controls.css' );
	}
}

// Doing this customizer thang!
the_writers_blog_Customize::get_instance();